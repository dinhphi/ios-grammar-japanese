//
//  DetailGrammar.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "DetailGrammar.h"
#import "DataEnDeCrypt.h"
#import "ADObject.h"


@interface DetailGrammar () {
    UIBarButtonItem *bookmarkIcon;
    UIBarButtonItem *bookmarkDisable;
}
@end

@implementation DetailGrammar

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];

    
    [self.descriptionWord setScrollEnabled:NO];
    [self.content setScrollEnabled:NO];
    [self.imi setScrollEnabled:NO];
    [self.imi setUserInteractionEnabled:NO];
    [self.content setUserInteractionEnabled:NO];
    [self.descriptionWord setUserInteractionEnabled:NO];
    
    
    CGSize scrollableSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.detailScrollView.contentSize.height);
    [self.detailScrollView setContentSize:scrollableSize];
    [self.detailScrollView setShowsHorizontalScrollIndicator:NO];
    [self.detailScrollView setDelegate:self];
    [self detailWithGrammar:self.grammar];
    UIImage *bookmarkIconImg = [UIImage imageNamed:@"bookmark-32.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(0.0, 0, 32, 32);
    [button setBackgroundImage:bookmarkIconImg forState:UIControlStateNormal];
    [button addTarget:self action:@selector(bookmarkGramar) forControlEvents:UIControlEventTouchUpInside];
    
    bookmarkIcon = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    [bookmarkIcon setTintColor:[UIColor yellowColor]];
    
    bookmarkDisable = [[UIBarButtonItem alloc] initWithImage:bookmarkIconImg style:UIBarButtonItemStyleDone target:self action:@selector(bookmarkGramar)] ;
    [bookmarkDisable setTintColor:[UIColor grayColor]];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
}
- (void) setBookMarkIcon {
    if(self.grammar.bookmark == 1) {
        [self.navigationItem setRightBarButtonItem:bookmarkIcon];
    } else {
        [self.navigationItem setRightBarButtonItem:bookmarkDisable];
    }

}
- (void) bookmarkGramar {
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    if(self.grammar.bookmark == 0) {
        self.grammar.bookmark = 1;
    } else {
        self.grammar.bookmark = 0;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *todayString = [dateFormatter stringFromDate:[NSDate date]];
    NSString *query = [NSString stringWithFormat:@"update grammar_all set bookmark=%d, bm_datetime='%@' where word='%@'", self.grammar.bookmark,todayString,self.word.text];
    
    // Execute the query.
    [self.dbManager executeQuery:query];
    [self setBookMarkIcon];
}

- (void)viewWillAppear:(BOOL)animated {
      NSLog(@"viewWillAppear");
    [self.detailScrollView setContentOffset:
     CGPointMake(0, -self.detailScrollView.contentInset.top) animated:YES];
    [super viewWillAppear:animated];
    [self setBookMarkIcon];
}

- (void)detailWithGrammar:(Grammar *)grammar {
    if(!self.grammar) {
        self.grammar = grammar;
        return;
    }
    self.grammar = grammar;

    [self.word setTextColor:[UIColor blueColor]];
    
    //convert contents json
    NSString *contentJson = [DataEnDeCrypt Decrypt:grammar.content];
    NSError *error;
    NSDictionary *contentDict = [NSJSONSerialization JSONObjectWithData:[contentJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    self.word.text = [contentDict objectForKey:@"word"];
    self.imi.text = [contentDict objectForKey:@"mean"];
    [self.imi sizeToFit];
    
     self.useLabel.text = [contentDict objectForKey:@"formation"];
//    self.descriptionWord.text = [contentDict objectForKey:@"description"];
    
    NSArray *descriptionArr = [contentDict objectForKey:@"description"];
    NSMutableString *strComplex = [[NSMutableString alloc] init];
    NSMutableString *strDescription = [[NSMutableString alloc] init];
    for (int i=0; i<[descriptionArr count]; i++) {
         //get
        NSDictionary *descriptionDic = [descriptionArr objectAtIndex:i];
        if([[descriptionDic objectForKey:@"type"] isEqual: @"simple"]) {
            [strDescription appendString:[descriptionDic objectForKey:@"content"]];
        } else if([[descriptionDic objectForKey:@"type"] isEqual: @"complex"]) {
            
            [strComplex appendString:@"<span style=\"color:blue; font-family: San Francisco; font-size: 17\">注意：</span><br>"];
            //get other code
            NSArray *otherArr = [descriptionDic objectForKey:@"other"];
             for (int j=0; j<[otherArr count]; j++) {
                 [strComplex appendString:[otherArr objectAtIndex:j]];
             }
            [strComplex appendString:@"<br>"];
            //get example
            NSArray *examArr = [descriptionDic objectForKey:@"example"];
           
            for (int i=0; i<[examArr count]; i++) {
                NSDictionary *exam = [examArr objectAtIndex:i];
                //kanji:
                NSString *kanji = [exam objectForKey:@"ex_kanji"];
                if(kanji.length > 0) {
                    [strComplex appendFormat:@"<span style=\"color:red; font-family: San Francisco; font-size: 17\">%d. %@</span><br>",i+1,kanji];
                }
                //hira
                NSString *hira = [exam objectForKey:@"ex_hira"];
                if(hira.length > 0) {
                    [strComplex appendFormat:@"<span style=\"color:gray; font-family: San Francisco; font-size: 10\">%@</span><br>",hira];
                }
                //vn
                NSString *vn = [exam objectForKey:@"ex_vn"];
                if(vn.length > 0) {
                    [strComplex appendFormat:@"<span style=\"color:black; font-family: San Francisco; font-size: 17\">%@</span><br>",vn];
                }
                [strComplex appendString:@"<br>"];
            }

        }
     }
    
    self.descriptionWord.text = strDescription;
    
    NSArray *examArr = [contentDict objectForKey:@"example"];
    self.content.text = @"";
    NSMutableString *str = [[NSMutableString alloc] init];
    for (int i=0; i<[examArr count]; i++) {
            NSDictionary *exam = [examArr objectAtIndex:i];
            //kanji:
            NSString *kanji = [exam objectForKey:@"ex_kanji"];
            if(kanji.length > 0) {
                [str appendFormat:@"<span style=\"color:red; font-family: San Francisco; font-size: 17\">%d. %@</span><br>",i+1,kanji];
            }
            //hira
            NSString *hira = [exam objectForKey:@"ex_hira"];
            if(hira.length > 0) {
                 [str appendFormat:@"<span style=\"color:gray; font-family: San Francisco; font-size: 10\">%@</span><br>",hira];
            }
            //vn
             NSString *vn = [exam objectForKey:@"ex_vn"];
            if(vn.length > 0) {
                [str appendFormat:@"<span style=\"color:black; font-family: San Francisco; font-size: 17\">%@</span><br>",vn];
            }
        [str appendString:@"<br>"];
    }
    
    [str appendString:strComplex];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.content.attributedText = attributedString;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
