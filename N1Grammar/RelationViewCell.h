//
//  RelationViewCell.h
//  N1Grammar
//
//  Created by Dinh Phi on 2/20/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelationWord.h"
#import "RelationWordButton.h"

@interface RelationViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet RelationWordButton *relation;

@end
