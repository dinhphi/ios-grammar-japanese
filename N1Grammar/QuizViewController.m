//
//  QuizViewController.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/10/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "QuizViewController.h"
#import "Quiz.h"
#import "ADObject.h"
#import "CustomIOSAlertView.h"
#import "RelationGrammarView.h"
#import "RelationViewCell.h"
#import "RelationWord.h"
#import "RelationDataModel.h"

@interface QuizViewController () {
    Quiz *quiz;
    CustomIOSAlertView *alertView;
    NSArray *recipePhotos;
    RelationGrammarView *relation;
    UIButton *displayButton;
    UIButton *unDisplayButton;
}

@end

@implementation QuizViewController
@synthesize ans1 = ans1;
@synthesize ans2 = ans2;
@synthesize ans3 = ans3;
@synthesize ans4 = ans4;
@synthesize next = next;
@synthesize level = levelLabel;
@synthesize quest = quest;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];
    
    
    UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [settingBtn setFrame:CGRectMake(0, 0, 32, 32)];
    [settingBtn setBackgroundImage:[UIImage imageNamed:@"settings-32.png"] forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(moveSetting) forControlEvents:UIControlEventTouchUpInside];
    
    displayButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [displayButton setFrame:CGRectMake(0, 0, 32, 32)];
    UIImage *img = [[UIImage imageNamed:@"off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [displayButton setBackgroundImage:img forState:UIControlStateNormal];
    [displayButton setTintColor:[[self view]tintColor]];
    
    
    unDisplayButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [unDisplayButton setFrame:CGRectMake(0, 0, 32, 32)];
    [unDisplayButton setBackgroundImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    
    [displayButton addTarget:self action:@selector(chaneDisplay) forControlEvents:UIControlEventTouchUpInside];
    [unDisplayButton addTarget:self action:@selector(chaneDisplay) forControlEvents:UIControlEventTouchUpInside];
    
    self.settingController = [[SettingViewController alloc] init];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:settingBtn]];

    
    [ans1.layer setBorderWidth:1.0f];
    [ans1.layer setBorderColor:[UIColor grayColor].CGColor];
    ans1.layer.cornerRadius = 5.0;
    ans1.layer.masksToBounds = true;
    
    [ans2.layer setBorderWidth:1.0f];
    [ans2.layer setBorderColor:[UIColor grayColor].CGColor];
    ans2.layer.cornerRadius = 5.0;
    ans2.layer.masksToBounds = true;
    
    [ans3.layer setBorderWidth:1.0f];
    [ans3.layer setBorderColor:[UIColor grayColor].CGColor];
    ans3.layer.cornerRadius = 5.0;
    ans3.layer.masksToBounds = true;
    
    [ans4.layer setBorderWidth:1.0f];
    [ans4.layer setBorderColor:[UIColor grayColor].CGColor];
    ans4.layer.cornerRadius = 5.0;
    ans4.layer.masksToBounds = true;
    
    [[ans1 titleLabel] setNumberOfLines:0];
    [[ans2 titleLabel] setNumberOfLines:0];
    [[ans3 titleLabel] setNumberOfLines:0];
    [[ans4 titleLabel] setNumberOfLines:0];
    
    
    [ans1 addTarget:self action:@selector(answer:) forControlEvents:UIControlEventTouchUpInside];
    [ans2 addTarget:self action:@selector(answer:) forControlEvents:UIControlEventTouchUpInside];
    [ans3 addTarget:self action:@selector(answer:) forControlEvents:UIControlEventTouchUpInside];
    [ans4 addTarget:self action:@selector(answer:) forControlEvents:UIControlEventTouchUpInside];
    
    quiz = [[Quiz alloc] init];
    [self loadData];
    [next addTarget:self action:@selector(nextQuiz) forControlEvents:UIControlEventTouchUpInside];
    
    next.layer.cornerRadius = 5.0;
    next.layer.masksToBounds = true;
    next.layer.borderWidth = 1.0;
    next.layer.borderColor = [UIColor grayColor].CGColor;
    
    alertView = [[CustomIOSAlertView alloc] init];
    
    relation = [[RelationGrammarView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    
    [alertView setContainerView:relation];
    
    [self.relationGrid registerNib:[UINib nibWithNibName:@"RelationViewCell" bundle:nil] forCellWithReuseIdentifier:@"RelationViewCell"];
    
    [self.relationGrid setBackgroundColor:[UIColor clearColor]];
    
    [self changeDisplay:[RelationDataModel getDisplayMode]];
    // Do any additional setup after loading the view from its nib.
}

-(void)changeDisplay:(bool)isDisplay {
    if(isDisplay) {
        [self.relationGrid setHidden:NO];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:displayButton]];
    } else {
        [self.relationGrid setHidden:YES];
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:unDisplayButton]];
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return quiz.quizDetail.realationWords.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"RelationViewCell";
    
    RelationViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if(cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RelationViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    RelationWord *relationWord = (RelationWord *)[quiz.quizDetail.realationWords objectAtIndex:indexPath.row];
    
    [cell.relation setTitle:relationWord.word forState:UIControlStateNormal];
    [[cell.relation layer] setBorderWidth:1.0f];
    [[cell.relation layer] setBorderColor:[UIColor grayColor].CGColor];
    cell.relation.relationWord = relationWord;
    
    
    [cell.relation addTarget:self action:@selector(showDialog:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

//when click into cell of relation
- (void)showDialog:(id)sender {
    RelationWordButton *relationWordButton = (RelationWordButton *)sender ;
    [relation loadData:relationWordButton.relationWord];
    NSLog(@"ID: %@ word: %@",relationWordButton.relationWord.idw,relationWordButton.relationWord.word);
    [alertView show];
}


- (void)chaneDisplay {
    bool oldDisplaySetting = [RelationDataModel getDisplayMode];
    bool newDisplaySetting = !oldDisplaySetting;
    [RelationDataModel setDisplayMode:newDisplaySetting];
    [self changeDisplay:newDisplaySetting];
}


- (void)moveSetting {
    [self.navigationController pushViewController:self.settingController animated:YES];
}

- (void)nextQuiz {
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];
//    [self setVisable:NO];
    [ans1 setBackgroundColor:[UIColor clearColor]];
    [ans2 setBackgroundColor:[UIColor clearColor]];
    [ans3 setBackgroundColor:[UIColor clearColor]];
    [ans4 setBackgroundColor:[UIColor clearColor]];
    [ans1.layer setBorderWidth:1.0f];
    [ans1.layer setBorderColor:[UIColor grayColor].CGColor];
    [ans2.layer setBorderWidth:1.0f];
    [ans2.layer setBorderColor:[UIColor grayColor].CGColor];
    [ans3.layer setBorderWidth:1.0f];
    [ans3.layer setBorderColor:[UIColor grayColor].CGColor];
    [ans4.layer setBorderWidth:1.0f];
    [ans4.layer setBorderColor:[UIColor grayColor].CGColor];
    
    
    
    [self loadData];
    [self.relationGrid reloadData];
    
}
- (void)viewWillAppear:(BOOL)animated {
    int level = [[[SettingViewController alloc] init] loadLevel];
    
    NSLog(@"levelSetting:%d",level);
    NSLog(@"levelQuiz:%d",quiz.level );
    
    if(quiz.level != level) {
        [self nextQuiz];
        quiz.level = level;
    }
}

-(void)loadData {
    //load level
    int level = [[[SettingViewController alloc] init] loadLevel];
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"]; //Refactory

    //get count of level
    NSString *query = @"";
    if(level == 0) {
        query = @"select count(*) as count,1 as min,level from grammar_quiz";
    } else {
        query = [NSString stringWithFormat:@"select count(*) as count,min(key) as min,level from grammar_quiz where level = %d group by level",level];
    }
   
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSInteger indexCount = [self.dbManager.arrColumnNames indexOfObject:@"count"];
    NSInteger indexMin = [self.dbManager.arrColumnNames indexOfObject:@"min"];
   
    int count = 0;
    int min = 0;
    for(int i=0 ; i< [resultArray count];i++) {
        count = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexCount] intValue];
        min   = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexMin] intValue];
        
    }
    
    //get random
    int keyRandom = arc4random_uniform((u_int32_t )count) + min;
    NSLog(@"key=%d",keyRandom);
    query = [NSString stringWithFormat:@"select * from grammar_quiz where key = %d",keyRandom];
    // Execute the query.
    NSArray *resultArrayOfOneQuiz = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    NSInteger indexContent = [self.dbManager.arrColumnNames indexOfObject:@"question"];
    NSInteger indexLevel = [self.dbManager.arrColumnNames indexOfObject:@"level"];
    NSInteger indexRelation = [self.dbManager.arrColumnNames indexOfObject:@"relation"];
    
    int levelInt = 0;
    for(int i=0 ; i< [resultArray count];i++) {
        NSString *content = [[resultArrayOfOneQuiz objectAtIndex:i] objectAtIndex:indexContent];
        levelInt = [(NSString *)[[resultArrayOfOneQuiz objectAtIndex:i] objectAtIndex:indexLevel] intValue];
        NSString *relation = [[resultArrayOfOneQuiz objectAtIndex:i] objectAtIndex:indexRelation];
        
        [quiz initWithKey:keyRandom content:content level:level relation:relation];
    }
    NSString *levelStr = [NSString stringWithFormat:@"All Level (N%d)",levelInt];
    if(level != 0) {
        levelStr = [NSString stringWithFormat:@"N%d",level];
    }
    self.navigationItem.title = levelStr;
    
    [quiz getDetailQuiz];
    [quiz.quizDetail shufferArray];
    
    //add detail to label:
    [quest setText:quiz.quizDetail.quest];
    NSString *t1 = [[quiz.quizDetail.ansArray objectAtIndex:0] objectAtIndex:0];
    NSString *t2 = [[quiz.quizDetail.ansArray objectAtIndex:1] objectAtIndex:0];
    NSString *t3 = [[quiz.quizDetail.ansArray objectAtIndex:2] objectAtIndex:0];
    NSString *t4 = [[quiz.quizDetail.ansArray objectAtIndex:3] objectAtIndex:0];
    [ans1 setTitle:t1 forState:UIControlStateNormal];
    [ans2 setTitle:t2 forState:UIControlStateNormal];
    [ans3 setTitle:t3 forState:UIControlStateNormal];
    [ans4 setTitle:t4 forState:UIControlStateNormal];

   
}

- (void)setVisable:(BOOL)visiable {
    [ans1 setHidden:visiable];
    [ans2 setHidden:visiable];
    [ans3 setHidden:visiable];
    [ans4 setHidden:visiable];
    if(visiable) {
        [quest setText:@"NO DATA"];
    }
}

- (void)answer:(id)sender {
    UIButton *button = (UIButton *)sender;
    if([[[quiz.quizDetail.ansArray objectAtIndex:button.tag] objectAtIndex:1]  isEqual: @"1"]) {
        [button.layer setBorderWidth:2.0f];
        [button.layer setBorderColor:[UIColor colorWithRed:70.0/255.0 green:255.0/255.0 blue:89.0/255.0 alpha:0.7].CGColor];
        [button setBackgroundColor:[UIColor colorWithRed:70.0/255.0 green:255.0/255.0 blue:89.0/255.0 alpha:0.7]];
    } else {
        [button.layer setBorderWidth:2.0f];
        [button.layer setBorderColor:[UIColor colorWithRed:255.0/255.0 green:160.0/255.0 blue:143.0/255.0 alpha:0.7].CGColor];
        [button setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:160.0/255.0 blue:143.0/255.0 alpha:0.7]];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
