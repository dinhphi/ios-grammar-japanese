//
//  DataEnDeCrypt.m
//  N1Grammar
//
//  Created by Dinh Phi on 10/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "DataEnDeCrypt.h"
#import "RNEncryptor.h"
#import "RNDecryptor.h"


#define PASSWORD @"devsoftpro20160310"

@implementation DataEnDeCrypt
+(NSString *)Decrypt:(NSString *)encryptedStr {
    NSError *error;
    NSData *data = [RNDecryptor decryptData:[[NSData alloc] initWithBase64EncodedString:encryptedStr
                                                                                options:NSDataBase64DecodingIgnoreUnknownCharacters]
                               withPassword:PASSWORD
                                      error:&error];
    
    NSString *decryptStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (decryptStr) {
        return decryptStr;
    }
    return NULL;
}

@end
