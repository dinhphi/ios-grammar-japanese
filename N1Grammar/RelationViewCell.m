//
//  RelationViewCell.m
//  N1Grammar
//
//  Created by Dinh Phi on 2/20/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import "RelationViewCell.h"

@implementation RelationViewCell

- (void)awakeFromNib {
    // Initialization code
    self.relation.titleLabel.adjustsFontSizeToFitWidth = YES;
}

@end
