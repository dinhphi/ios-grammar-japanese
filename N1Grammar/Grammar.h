//
//  Grammar.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Grammar : NSObject
@property NSString *word;
@property NSString *imi;
@property NSString *content;
@property int level;
@property int bookmark;


-(instancetype)initWithWord:(NSString *)word imi:(NSString *)imi content:(NSString *)content bookmark:(int) bookmark level:(int)level;

@end
