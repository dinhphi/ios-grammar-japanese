//
//  ADObject.h
//  N1Grammar
//
//  Created by Dinh Phi on 11/16/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMobileAds;

@interface ADObject : NSObject
+(void)requestToDisplayADWith:(id)viewcontroller googleAD:(GADBannerView *)bannerView;
@end
