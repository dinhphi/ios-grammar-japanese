//
//  BookMarkViewController.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/7/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "Grammar.h"
#import "DetailGrammar.h"
#import "SettingViewController.h"
#import "grammarCell.h"

@interface BookMarkViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *bookmarktables;

@property (nonatomic) DBManager *dbManager;
@property (nonatomic) SettingViewController *settingController;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
