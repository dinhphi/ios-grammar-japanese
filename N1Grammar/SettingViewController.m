//
//  SettingViewController.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/9/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "SettingViewController.h"
#import "ADObject.h"

@interface SettingViewController () {
}

@end

@implementation SettingViewController
@synthesize switchAllBtn = switchAllBtn;
@synthesize switchN1Btn = switchN1Btn;
@synthesize switchN2Btn = switchN2Btn;
@synthesize switchN3Btn = switchN3Btn;
@synthesize switchN4Btn = switchN4Btn;
@synthesize switchN5Btn = switchN5Btn;
@synthesize model = model;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];
    
    
    [switchAllBtn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [switchN1Btn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [switchN2Btn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [switchN3Btn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [switchN4Btn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [switchN5Btn addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    [self loadDataFromDB];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [self loadDataFromDB];
}

-(void)loadDataFromDB {
    int level = [self loadLevel];
    switch (level) {
        case 0:
            [switchAllBtn setOn:YES animated:YES];
            [self setState:switchAllBtn];
            break;
        case 1:
            [switchN1Btn setOn:YES animated:YES];
            [self setState:switchN1Btn];
            break;
        case 2:
            [switchN2Btn setOn:YES animated:YES];
            [self setState:switchN2Btn];
            break;
        case 3:
            [switchN3Btn setOn:YES animated:YES];
            [self setState:switchN3Btn];
            break;
        case 4:
            [switchN4Btn setOn:YES animated:YES];
            [self setState:switchN4Btn];
            break;
        case 5:
            [switchN5Btn setOn:YES animated:YES];
            [self setState:switchN5Btn];
            break;
        default:
            break;
    }

}

-(int)loadLevel {
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    NSString *query = @"select * from state_mode";
    
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSInteger indexOfLevel = [self.dbManager.arrColumnNames indexOfObject:@"level"];
    NSString *levelStr = @"";
    for(int i=0 ; i< [resultArray count];i++) {
        levelStr = [[resultArray objectAtIndex:i] objectAtIndex:indexOfLevel];
    }
    int level = [levelStr intValue];
    NSLog(@"level:%d",level);
    return level;
}

-(void)saveModel {
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    NSString *query = [NSString stringWithFormat:@"update state_mode set level=%d",self.model];
    
    // Execute the query.
    [self.dbManager executeQuery:query];

}

-(void)setState:(id)sender{
    UISwitch *btn = (UISwitch *)sender;
    
    if (btn != switchAllBtn) {
         [switchAllBtn setOn:NO animated:YES];
    }
    if (btn != switchN1Btn) {
        [switchN1Btn setOn:NO animated:YES];
    }

    if (btn != switchN2Btn) {
        [switchN2Btn setOn:NO animated:YES];
    }

    if (btn != switchN3Btn) {
        [switchN3Btn setOn:NO animated:YES];
    }

    if (btn != switchN4Btn) {
        [switchN4Btn setOn:NO animated:YES];
    }
    
    if (btn != switchN5Btn) {
        [switchN5Btn setOn:NO animated:YES];
    }
    
    if (btn == switchAllBtn) {
        model = 0;
    }
    if (btn == switchN1Btn) {
        model = 1;
    }
    
    if (btn == switchN2Btn) {
        model = 2;
    }
    
    if (btn == switchN3Btn) {
        model = 3;
    }
    
    if (btn == switchN4Btn) {
        model = 4;
    }
    
    if (btn == switchN5Btn) {
        model = 5;
    }
    [self saveModel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
