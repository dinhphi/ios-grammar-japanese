//
//  DetailGrammar.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Grammar.h"
#import "DBManager.h"
@import GoogleMobileAds;

@interface DetailGrammar : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *word;
@property (weak, nonatomic) IBOutlet UITextView *imi;
@property (weak, nonatomic) IBOutlet UITextView *descriptionWord;
@property (weak, nonatomic) IBOutlet UITextView *content;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentWidth;
@property (weak, nonatomic) IBOutlet UILabel *useLabel;


@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@property (nonatomic, strong) DBManager *dbManager;
@property Grammar *grammar;

- (void)detailWithGrammar:(Grammar *)grammar;
@end
