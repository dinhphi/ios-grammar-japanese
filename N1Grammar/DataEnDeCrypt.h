//
//  DataEnDeCrypt.h
//  N1Grammar
//
//  Created by Dinh Phi on 10/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataEnDeCrypt : NSObject

+(NSString *)Decrypt:(NSString *)encryptedStr;
@end
