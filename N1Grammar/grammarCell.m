//
//  grammarCell.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/15/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "grammarCell.h"

@implementation grammarCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)initWithWord:(NSString *)word imi:(NSString *)mean level:(int)level {
    [self.word setText:word];
    [self.imi setText:mean];
    [self.level setText:[NSString stringWithFormat:@"N%d",level]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
