//
//  ViewController.h
//  N1Grammar
//
//  Created by Dinh Phi on 8/29/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "SettingViewController.h"
#import "QuizViewController.h"
#import "grammarCell.h"
@import GoogleMobileAds;

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property NSString *searchText;

@property (weak, nonatomic) IBOutlet UITableView *grammarTable;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic) SettingViewController *settingController;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end

