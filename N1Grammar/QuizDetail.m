//
//  QuizDetail.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/11/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "QuizDetail.h"
#import "RelationWord.h"

@implementation QuizDetail
@synthesize ansArray = ansArray;

-(NSMutableArray *) getRelationWords:(NSString *) relations{
    
    NSMutableArray *words = [[NSMutableArray alloc] init];
    
    NSData *dataContent = [relations dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *contentJson = [NSJSONSerialization JSONObjectWithData:dataContent options:0 error:nil];
    for (NSString *key in [contentJson allKeys]) {
        RelationWord *releationWord = [[RelationWord alloc] init];
        releationWord.idw = key;
        releationWord.word = [contentJson objectForKey:key];
        [words addObject:releationWord];
    }
    
    return words;
}

-(void)initWithContent:(NSString *)content Relation:(NSString *)relation{
    //parse content:
    NSData *dataContent = [content dataUsingEncoding:NSUTF8StringEncoding];
    id contentJson = [NSJSONSerialization JSONObjectWithData:dataContent options:0 error:nil];
    self.quest = [contentJson objectForKey:@"ques"];
    int correct = [[contentJson objectForKey:@"correct_ans"] intValue];
    //ans 1
    NSMutableArray *ans1 = [[NSMutableArray alloc] init];
    [ans1 addObject:[contentJson objectForKey:@"ans1"]];
    if(correct ==1) {
        [ans1 addObject:@"1"];
    } else {
        [ans1 addObject:@"0"];
    }
    
    //ans2
    NSMutableArray *ans2 = [[NSMutableArray alloc] init];
    [ans2 addObject:[contentJson objectForKey:@"ans2"]];
    if(correct == 2) {
        [ans2 addObject:@"1"];
    } else {
        [ans2 addObject:@"0"];
    }
    
    //ans3
    NSMutableArray *ans3 = [[NSMutableArray alloc] init];
    [ans3 addObject:[contentJson objectForKey:@"ans3"]];
    if(correct == 3) {
        [ans3 addObject:@"1"];
    } else {
        [ans3 addObject:@"0"];
    }
    
    //ans4
    NSMutableArray *ans4 = [[NSMutableArray alloc] init];
    [ans4 addObject:[contentJson objectForKey:@"ans4"]];
    if(correct == 4) {
        [ans4 addObject:@"1"];
    } else {
        [ans4 addObject:@"0"];
    }
    
    //array ans
    ansArray = [[NSMutableArray alloc] init];
    [ansArray addObject:ans1];
    [ansArray addObject:ans2];
    [ansArray addObject:ans3];
    [ansArray addObject:ans4];
    
    self.realationWords = [self getRelationWords:relation];
}

-(void)shufferArray {
    NSUInteger count = [ansArray count];
    if (count < 1) return;
    for (NSUInteger i = 0; i < count - 1; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [ansArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}
@end
