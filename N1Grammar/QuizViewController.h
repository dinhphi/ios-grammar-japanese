//
//  QuizViewController.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/10/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "SettingViewController.h"

@interface QuizViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *level;
@property (weak, nonatomic) IBOutlet UITextView *quest;
@property (weak, nonatomic) IBOutlet UIButton *ans1;
@property (weak, nonatomic) IBOutlet UIButton *ans2;
@property (weak, nonatomic) IBOutlet UIButton *ans3;
@property (weak, nonatomic) IBOutlet UIButton *ans4;
@property (weak, nonatomic) IBOutlet UIButton *next;
@property (weak, nonatomic) IBOutlet UICollectionView *relationGrid;


@property (nonatomic) DBManager *dbManager;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property (nonatomic) SettingViewController *settingController;
@end
