//
//  Quiz.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/11/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuizDetail.h"

@interface Quiz : NSObject
@property (nonatomic) int key;
@property (nonatomic) NSString *content;
@property (nonatomic) int level;
@property (nonatomic) QuizDetail *quizDetail;
@property (nonatomic) NSString *relation;

-(void)initWithKey:(int)key content:(NSString *)content level:(int)level relation:(NSString *)relation;
-(QuizDetail *)getDetailQuiz;
@end
