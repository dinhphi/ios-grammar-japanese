//
//  QuizDetail.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/11/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizDetail : NSObject
@property NSString *quest;
@property NSMutableArray *ansArray;
@property NSMutableArray *realationWords;
@property int correctIndex;
-(void)initWithContent:(NSString *)content Relation:(NSString *)relation;
-(void)shufferArray;
@end
