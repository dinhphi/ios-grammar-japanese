//
//  Quiz.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/11/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "Quiz.h"
#import "DataEnDeCrypt.h"

@implementation Quiz
@synthesize quizDetail = quizDetail;
-(void)initWithKey:(int)key content:(NSString *)content level:(int)level relation:(NSString *)relation {
    self.key = key;
    self.content = [DataEnDeCrypt Decrypt:content];
    self.level = level;
    self.relation = relation;
    
}
-(QuizDetail *)getDetailQuiz {
    if(!quizDetail) {
        quizDetail = [[QuizDetail alloc] init];
    }
    [quizDetail initWithContent:self.content Relation:self.relation];
    return quizDetail;
}
@end
