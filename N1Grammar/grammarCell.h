//
//  grammarCell.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/15/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface grammarCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *word;
@property (weak, nonatomic) IBOutlet UILabel *imi;
@property (weak, nonatomic) IBOutlet UILabel *level;

-(void)initWithWord:(NSString *)word imi:(NSString *)mean level:(int)level;

@end
