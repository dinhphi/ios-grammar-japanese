//
//  ViewController.m
//  N1Grammar
//
//  Created by Dinh Phi on 8/29/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "ViewController.h"
#import <sqlite3.h>
#import "Grammar.h"
#import "DetailGrammar.h"
#import "BookMarkViewController.h"
#import "QuizViewController.h"
#import "ADObject.h"


@interface ViewController () {
    NSMutableArray *listGramar;
    NSMutableArray *listSearch;
    DetailGrammar *detailGrammarController;
    BookMarkViewController *bookmarkController;
    QuizViewController *quizController;
    int currentLevel;
    NSString *searchTextStr;
    bool isSearching;
}

@end

@implementation ViewController
@synthesize grammarTable = grammarTable;
@synthesize searchBar = searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];

    [grammarTable setDelegate:self];
    [grammarTable setDataSource:self];
    grammarTable.rowHeight = UITableViewAutomaticDimension;
    grammarTable.estimatedRowHeight = 80;
    [searchBar setDelegate:self];
    listGramar = [[NSMutableArray alloc] init];
    listSearch = [[NSMutableArray alloc] init];
    currentLevel = -1;
    isSearching = false;
    
//    [self.tabBarController setDelegate:self];
    
    
    bookmarkController = [[BookMarkViewController alloc] init];
    quizController = [[QuizViewController alloc] init];
  
    NSMutableArray *allviews=[[NSMutableArray alloc] initWithArray:[self.tabBarController viewControllers]];
    UINavigationController *narControl = (UINavigationController *)[allviews objectAtIndex:1];
    [narControl pushViewController:bookmarkController animated:YES];
    
    UINavigationController *narQuizControl = (UINavigationController *)[allviews objectAtIndex:2];
    [narQuizControl pushViewController:quizController animated:YES];
    
    
    
    self.settingController = [[SettingViewController alloc] init];
    UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [settingBtn setFrame:CGRectMake(0, 0, 32, 32)];
    [settingBtn setBackgroundImage:[UIImage imageNamed:@"settings-32.png"] forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(moveSetting) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:settingBtn]];
    
        // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated {
    int settingLevel = [self.settingController loadLevel];
    if(currentLevel != settingLevel) {
        NSLog(@"Change level");
        currentLevel = settingLevel;
        [self loadDataFromDB];
        [listSearch addObjectsFromArray:listGramar ];
        [grammarTable reloadData];
    }
}

- (void)moveSetting {
    [self.navigationController pushViewController:self.settingController animated:YES];
}

- (void)loadDataFromDB {
    [listGramar removeAllObjects];
    [listSearch removeAllObjects];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    NSString *query = @"";
    if(currentLevel == 0) {
        query = @"select * from grammar_all order by word";
    } else {
        query = [NSString stringWithFormat:@"select * from grammar_all where level=%d order by word",currentLevel];
    }
    
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSInteger indexOfWord = [self.dbManager.arrColumnNames indexOfObject:@"word"];
    NSInteger indexOfImi = [self.dbManager.arrColumnNames indexOfObject:@"mean"];
    NSInteger indexOfContent = [self.dbManager.arrColumnNames indexOfObject:@"content"];
    NSInteger indexOfBookMark = [self.dbManager.arrColumnNames indexOfObject:@"bookmark"];
    NSInteger indexOfLevel = [self.dbManager.arrColumnNames indexOfObject:@"level"];
    for(int i=0 ; i< [resultArray count];i++) {
        NSString *word = [[resultArray objectAtIndex:i] objectAtIndex:indexOfWord];
        NSString *imi = [[resultArray objectAtIndex:i] objectAtIndex:indexOfImi];
        NSString *content = [[resultArray objectAtIndex:i] objectAtIndex:indexOfContent];
        int bookmark = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfBookMark] intValue];
        int level =[(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfLevel] intValue];
        Grammar *grammar = [[Grammar alloc] initWithWord:word imi:imi content:content bookmark:bookmark level:level];
        [listGramar addObject:grammar];
    }
}

- (NSMutableArray *)loadArrayDataFromSearchQuery:(NSString *)pattern {
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    NSString *query = [NSString stringWithFormat:@"select word,mean,level,content,bookmark,bm_datetime from grammar_all WHERE (word like ?) OR (romanji like ?) GROUP BY word,mean,level,content,bookmark,bm_datetime ORDER BY word "];
    
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query search:pattern]];
    NSInteger indexOfWord = [self.dbManager.arrColumnNames indexOfObject:@"word"];
    NSInteger indexOfImi = [self.dbManager.arrColumnNames indexOfObject:@"mean"];
    NSInteger indexOfContent = [self.dbManager.arrColumnNames indexOfObject:@"content"];
    NSInteger indexOfBookMark = [self.dbManager.arrColumnNames indexOfObject:@"bookmark"];
    NSInteger indexOfLevel = [self.dbManager.arrColumnNames indexOfObject:@"level"];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for(int i=0 ; i< [resultArray count];i++) {
        NSString *word = [[resultArray objectAtIndex:i] objectAtIndex:indexOfWord];
        NSString *imi = [[resultArray objectAtIndex:i] objectAtIndex:indexOfImi];
        NSString *content = [[resultArray objectAtIndex:i] objectAtIndex:indexOfContent];
        int bookmark = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfBookMark] intValue];
        int level =[(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfLevel] intValue];

        Grammar *grammar = [[Grammar alloc] initWithWord:word imi:imi content:content bookmark:bookmark level:level];
        [result addObject:grammar];
    }
    return result;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listSearch count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *simpleTableIdentifier = @"grammarcell";
    grammarCell *cell = (grammarCell *) [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"grammarCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    NSString *word = ((Grammar *)[listSearch objectAtIndex:indexPath.row]).word;
    NSString *mean = ((Grammar *)[listSearch objectAtIndex:indexPath.row]).imi;
    int level = ((Grammar *)[listSearch objectAtIndex:indexPath.row]).level;
    
    [cell initWithWord:word imi:mean level:level];
    
    [cell.word setTextColor:[UIColor blackColor]];
    if(self.searchText.length > 0) {
        [self changeColorOfSearch:cell.word searchText:self.searchText];
    }
    
    return cell;
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    self.searchText = searchText;
    [self searchGrammar:searchText];
}

-(void)searchGrammar:(NSString *)searchText {
    if (listSearch.count > 0)
        [listSearch removeAllObjects];
    
    if ([searchText length] > 0)
    {
        listSearch = [self loadArrayDataFromSearchQuery:searchText];
    }
    else
    {
        [listSearch addObjectsFromArray:listGramar ];
    }
    
    [grammarTable reloadData];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    isSearching = true;
    dispatch_time_t *when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(100 * NSEC_PER_MSEC));
    dispatch_after(when, dispatch_get_main_queue(), ^(void){
        self.searchText =[searchBar.text mutableCopy];
        [self searchGrammar:self.searchText];
       
    });
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(onTick:) withObject:nil afterDelay:0.5];
    return YES;
}




-(void)onTick:(NSTimer *)timer {
    //do smth
    isSearching = false;
}



-(void)changeColorOfSearch:(UILabel *)word searchText:(NSString *)searchText {
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString: word.text];
    
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:[word.text rangeOfString:searchText]];
    [word setAttributedText: text];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Grammar *grammar = (Grammar *)[listSearch objectAtIndex:indexPath.row];
    if (!detailGrammarController) {
        detailGrammarController = [[DetailGrammar alloc] init];
        [self.tabBarController addChildViewController:detailGrammarController];
    }
    [detailGrammarController detailWithGrammar:grammar];
    [searchBar resignFirstResponder];
    detailGrammarController.hidesBottomBarWhenPushed = NO;
    self.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:detailGrammarController animated:YES];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)scrollViewDidScroll: (UIScrollView *)scrollView
{
    if(!isSearching) {
        [searchBar resignFirstResponder];
    }
}

//- (void)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
//    NSInteger selectIndex = [tabBarController.viewControllers indexOfObject:viewController];
//    if(selectIndex == 2)
////    if (viewController == [tabBarController.viewControllers objectAtIndex:1] ) {
////        if(!bookmarkController) {
////              bookmarkController = [[BookMarkViewController alloc] init];
////        }
//////        viewController = bookmarkController;
////    }
//    
//}

@end
