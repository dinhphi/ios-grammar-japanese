//
//  RelationWord.h
//  N1Grammar
//
//  Created by Dinh Phi on 2/20/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RelationWord : NSObject
@property NSString *idw;
@property NSString *word;
@end
