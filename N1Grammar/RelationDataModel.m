//
//  RelationDataModel.m
//  N1Grammar
//
//  Created by Dinh Phi on 2/20/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import "RelationDataModel.h"
#import "RelationDetail.h"
#import "DataEnDeCrypt.h"

@implementation RelationDataModel

+(RelationDetail *)getRelationDataWithKey:(NSString*) keyId {
    
    NSLog(keyId);
    RelationDetail *relationDetail = [[RelationDetail alloc] init];
    relationDetail.keyID = keyId;
    
    DBManager *dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"]; //Refactory
    
    //get count of level
    NSString *query = [NSString stringWithFormat:@"select * from jvtotal where key=%@",keyId];

    
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    NSInteger indexMean = [dbManager.arrColumnNames indexOfObject:@"mean"];
    NSInteger indexWord = [dbManager.arrColumnNames indexOfObject:@"word"];

    
    for(int i=0 ; i< [resultArray count];i++) {
        relationDetail.word = [DataEnDeCrypt Decrypt:[[resultArray objectAtIndex:i] objectAtIndex:indexWord]];
        relationDetail.mean = [DataEnDeCrypt Decrypt:[[resultArray objectAtIndex:i] objectAtIndex:indexMean]];
        
    }
    return relationDetail;

}

+(bool)getDisplayMode {
    
    DBManager *dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"]; //Refactory
    
    //get count of level
    NSString *query = [NSString stringWithFormat:@"select display_mode from state_mode"];
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    NSInteger indexDisplayMode = [dbManager.arrColumnNames indexOfObject:@"display_mode"];
    int isDisplay=0;
    for(int i=0 ; i< [resultArray count];i++) {
        isDisplay = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexDisplayMode] intValue];;
    }
    
    return isDisplay == 1? true:false;
}

+(void)setDisplayMode:(bool)isDisplay {
    
    DBManager *dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"]; //Refactory
    
    int isDisplayMode = isDisplay? 1:0;
    //get count of level
    NSString *query = [NSString stringWithFormat:@"update state_mode set display_mode=%d",isDisplayMode];
    // Execute the query.
    [dbManager executeQuery:query];
}

@end
