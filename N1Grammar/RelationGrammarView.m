//
//  RelationGrammarView.m
//  N1Grammar
//
//  Created by Dinh Phi on 2/13/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import "RelationGrammarView.h"
#import "RelationDetail.h"
#import "RelationDataModel.h"

@implementation RelationGrammarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}

-(void)initializeSubviews {
   id view = [[[NSBundle mainBundle]
                          loadNibNamed:NSStringFromClass([self class])
                          owner:self
                          options:nil] objectAtIndex:0];
    [view setFrame:CGRectMake(0, 0, 320, 400)];
    [self addSubview:view];
}

-(void)loadData:(RelationWord*) relationWord{
    RelationDetail *relationDetail = [RelationDataModel getRelationDataWithKey:relationWord.idw];
    
    NSError *error;
    NSDictionary *contentDict = [NSJSONSerialization JSONObjectWithData:[relationDetail.word dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    [self.hantu setText: [contentDict objectForKey:@"hv"]];
    self.hantu.adjustsFontSizeToFitWidth = YES;
    
    [self.kanji setText:[contentDict objectForKey:@"kanji"]];
    self.kanji.layer.borderColor = [UIColor redColor].CGColor;
    self.kanji.layer.borderWidth = 3.0f;
    self.kanji.layer.cornerRadius = 5.0;
    self.kanji.layer.masksToBounds = true;
    self.kanji.adjustsFontSizeToFitWidth = YES;
    UIEdgeInsets myLabelInsets = {0, 20, 0, 20};
    [self.kanji drawTextInRect:UIEdgeInsetsInsetRect([self.kanji frame], myLabelInsets)];
    
    [self.yomikata setText:[NSString stringWithFormat:@"%@ / %@",[contentDict objectForKey:@"kana"],[contentDict objectForKey:@"form"]]];
    
    [self.content setText:[self getMean:relationDetail.mean]];
    self.content.adjustsFontSizeToFitWidth = YES;
    NSString *detailWord = [self getWordDetail:relationDetail.mean];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[detailWord dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.wordDetail.attributedText = attributedString;
    
}

-(NSMutableString*)getMean:(NSString*)wordDetail {
    NSMutableString *mean = [[NSMutableString alloc] init];
    NSError *error;
    NSArray *detailArr = [NSJSONSerialization JSONObjectWithData:[wordDetail dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
     for (int i=0; i<[detailArr count]; i++) {
         NSDictionary *detail = [detailArr objectAtIndex:i];
         if([detail objectForKey:@"summary"] != NULL) {
             [mean appendString:[detail objectForKey:@"summary"]];
             if(i<[detailArr count] -1) {
                 [mean appendString:@" / "];
             }
         }
     }
    return mean;
}

-(NSMutableString*)getWordDetail:(NSString*)wordDetail {
    NSMutableString *detailStr = [[NSMutableString alloc] init];
    NSError *error;
    NSArray *detailArr = [NSJSONSerialization JSONObjectWithData:[wordDetail dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    for (int i=0; i<[detailArr count]; i++) {
        NSDictionary *detail = [detailArr objectAtIndex:i];
        
        if([[detail objectForKey:@"type"] isEqual:@"complex"]) {
            NSArray *examArr = [detail objectForKey:@"example"];
            for (int j=0; j<[examArr count]; j++) {
                NSDictionary *exam = [examArr objectAtIndex:j];
                //kanji:
                NSString *kanji = [exam objectForKey:@"jp"];
                if(kanji.length > 0) {
                    [detailStr appendFormat:@"<span style=\"color:red; font-family: San Francisco; font-size: 17\"> %@</span><br>",kanji];
                }
                //vn
                NSString *vn = [exam objectForKey:@"en_vn"];
                if(vn.length > 0) {
                    [detailStr appendFormat:@"<span style=\"color:black; font-family: San Francisco; font-size: 15\">%@</span><br>",vn];
                }
                [detailStr appendString:@"<br>"];
            }
        }
    }
    return detailStr;
}



@end
