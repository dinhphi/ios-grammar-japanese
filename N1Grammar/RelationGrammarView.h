//
//  RelationGrammarView.h
//  N1Grammar
//
//  Created by Dinh Phi on 2/13/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelationWord.h"

@interface RelationGrammarView : UIView
@property (weak, nonatomic) IBOutlet UILabel *hantu;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *yomikata;
@property (weak, nonatomic) IBOutlet UILabel *kanji;
@property (weak, nonatomic) IBOutlet UITextView *wordDetail;

-(void)loadData:(RelationWord*) relationWord;
-(NSMutableString*)getMean:(NSString*)wordDetail;
@end
