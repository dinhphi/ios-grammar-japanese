//
//  SettingViewController.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/9/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@import GoogleMobileAds;

@interface SettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *switchAllBtn;
@property (weak, nonatomic) IBOutlet UISwitch *switchN1Btn;
@property (weak, nonatomic) IBOutlet UISwitch *switchN2Btn;
@property (weak, nonatomic) IBOutlet UISwitch *switchN3Btn;
@property (weak, nonatomic) IBOutlet UISwitch *switchN4Btn;
@property (weak, nonatomic) IBOutlet UISwitch *switchN5Btn;

@property int model;
@property (nonatomic) DBManager *dbManager;

@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
-(int)loadLevel;
@end
