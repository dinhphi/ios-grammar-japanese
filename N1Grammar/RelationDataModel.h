//
//  RelationDataModel.h
//  N1Grammar
//
//  Created by Dinh Phi on 2/20/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"
#import "RelationDetail.h"

@interface RelationDataModel : NSObject
@property (nonatomic) DBManager *dbManager;

+(RelationDetail *)getRelationDataWithKey:(NSString*) key;
+(bool)getDisplayMode;
+(void)setDisplayMode:(bool)isDisplay;
@end
