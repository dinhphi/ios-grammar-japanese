//
//  BookMarkViewController.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/7/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "BookMarkViewController.h"
#import "ADObject.h"

@interface BookMarkViewController ()

@end

@implementation BookMarkViewController {
    NSMutableArray *bookmarkArray;
    DetailGrammar *detailGrammarController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ADObject requestToDisplayADWith:self googleAD:self.bannerView];

    [self.bookmarktables setDelegate:self];
    [self.bookmarktables setDataSource:self];
    self.bookmarktables.rowHeight = UITableViewAutomaticDimension;
    self.bookmarktables.estimatedRowHeight = 80;
    bookmarkArray = [[NSMutableArray alloc] init];
    
    UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [settingBtn setFrame:CGRectMake(0, 0, 32, 32)];
    [settingBtn setBackgroundImage:[UIImage imageNamed:@"settings-32.png"] forState:UIControlStateNormal];
    [settingBtn addTarget:self action:@selector(moveSetting) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:settingBtn]];
    self.settingController = [[SettingViewController alloc] init];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)moveSetting {
    [self.navigationController pushViewController:self.settingController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [self loadBookMarkData];
    [self.bookmarktables reloadData];
}

- (void)loadBookMarkData {
    [bookmarkArray removeAllObjects];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"grammarn1.db"];
    NSString *query = @"select * from grammar_all where bookmark=1 order by bm_datetime desc";
    // Execute the query.
    NSArray *resultArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSInteger indexOfWord = [self.dbManager.arrColumnNames indexOfObject:@"word"];
    NSInteger indexOfImi = [self.dbManager.arrColumnNames indexOfObject:@"mean"];
    NSInteger indexOfContent = [self.dbManager.arrColumnNames indexOfObject:@"content"];
    NSInteger indexOfBookMark = [self.dbManager.arrColumnNames indexOfObject:@"bookmark"];
    NSInteger indexOfLevel = [self.dbManager.arrColumnNames indexOfObject:@"level"];
    for(int i=0 ; i< [resultArray count];i++) {
        NSString *word = [[resultArray objectAtIndex:i] objectAtIndex:indexOfWord];
        NSString *imi = [[resultArray objectAtIndex:i] objectAtIndex:indexOfImi];
        NSString *content = [[resultArray objectAtIndex:i] objectAtIndex:indexOfContent];
        int bookmark = [(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfBookMark] intValue];
        int level =[(NSString *)[[resultArray objectAtIndex:i] objectAtIndex:indexOfLevel] intValue];
        Grammar *grammar = [[Grammar alloc] initWithWord:word imi:imi content:content bookmark:bookmark level:level];
        [bookmarkArray addObject:grammar];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [bookmarkArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"grammarcell";
    grammarCell *cell = (grammarCell *) [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"grammarCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    NSString *word = ((Grammar *)[bookmarkArray objectAtIndex:indexPath.row]).word;
    NSString *mean = ((Grammar *)[bookmarkArray objectAtIndex:indexPath.row]).imi;
    int level = ((Grammar *)[bookmarkArray objectAtIndex:indexPath.row]).level;
    [cell initWithWord:word imi:mean level:level];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Grammar *grammar = (Grammar *)[bookmarkArray objectAtIndex:indexPath.row];
    if (!detailGrammarController) {
        detailGrammarController = [[DetailGrammar alloc] init];
        [self.tabBarController addChildViewController:detailGrammarController];
    }
    [detailGrammarController detailWithGrammar:grammar];
    [self.navigationController pushViewController:detailGrammarController animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
