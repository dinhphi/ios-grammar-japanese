//
//  Grammar.m
//  N1Grammar
//
//  Created by Dinh Phi on 9/3/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "Grammar.h"

@implementation Grammar


-(instancetype)initWithWord:(NSString *)word imi:(NSString *)imi content:(NSString *)content bookmark:(int)bookmark level:(int)level{
    self = [super init];
    if(self) {
        self.word = word;
        self.imi = imi;
        self.content = content;
        self.bookmark = bookmark;
        self.level = level;
    }
    return self;
}

@end
