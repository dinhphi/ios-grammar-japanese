//
//  RelationWordButton.h
//  N1Grammar
//
//  Created by Dinh Phi on 3/22/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelationWord.h"
@interface RelationWordButton : UIButton
@property RelationWord *relationWord;
@end
