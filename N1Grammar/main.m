//
//  main.m
//  N1Grammar
//
//  Created by Dinh Phi on 8/29/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
