//
//  ADObject.m
//  N1Grammar
//
//  Created by Dinh Phi on 11/16/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import "ADObject.h"


@implementation ADObject
#ifdef DEBUG
//debug
static NSString *adUnitID = @"ca-app-pub-3940256099942544/2934735716";
#else
//release
static NSString *adUnitID = @"ca-app-pub-6237521114419791/2318097263";
#endif


+(void)requestToDisplayADWith:(id)viewcontroller googleAD:(GADBannerView *)bannerView {
    bannerView.adUnitID = adUnitID;
    bannerView.rootViewController = viewcontroller;
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    
    request.testDevices = @[kGADSimulatorID];
    
    [bannerView loadRequest:request];
    

}
@end
