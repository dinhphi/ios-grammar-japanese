//
//  RelationDetail.h
//  N1Grammar
//
//  Created by Dinh Phi on 2/21/17.
//  Copyright © 2017 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RelationDetail : NSObject
@property NSString *keyID;
@property NSString *word;
@property NSString *mean;

@end
