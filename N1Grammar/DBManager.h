//
//  DBManager.h
//  N1Grammar
//
//  Created by Dinh Phi on 9/2/16.
//  Copyright © 2016 Dinh Phi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;


-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

-(void)copyDatabaseIntoDocumentsDirectory;
-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;
-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable searchText:(NSString *)searchText;
-(NSArray *)loadDataFromDB:(NSString *)query;
-(NSArray *)loadDataFromDB:(NSString *)query search:(NSString *)textSearch;
-(void)executeQuery:(NSString *)query;

@end
